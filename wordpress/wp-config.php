<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'C:\Users\david\school\aspe0004\wordpress\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1/,K91)^49/L7a?(do;/G,;PlcQ(~*q6_v^kYo~wCKYh3M&tyA8ZS:}%O1{{oofc');
define('SECURE_AUTH_KEY',  'B4_Hw1%l~BIZjZQYv[<u2/G FV.T5:fCDM(bd 1y;uLp3Jc?kl&C0}TD(^}|<XuF');
define('LOGGED_IN_KEY',    'KXILZB`V2e+p)R{L$9+W:RP_eS*-!V#r1J1T^n<),.-vT$.}mjv@[rhwsW%O.*Z$');
define('NONCE_KEY',        ';tKssN>Xju].TwL$=! PoXFcLz#CMQ_buKf~0%wFu`_.s#jPb,}%R_;9o(X)p6Z,');
define('AUTH_SALT',        '>gt`>%7!TveKiL+e#R2Mm?PR)|}INh{D%S8n=onEp3-gf@!_;X~LZ.qyS6YTMBK!');
define('SECURE_AUTH_SALT', 'uQIc}{kkUO7}nW)P4BpadI{:zMELJqR-Wl8f9^2ky2JZ&iaD@VDOI.AZp%D[YP+5');
define('LOGGED_IN_SALT',   'jk`hGdOhD/[%~yBb0s0-eU<4]-5z)c!X=EW0e?Fc2mo>b7oVg?w*Q!n`& GaXNtd');
define('NONCE_SALT',       '7i#X i+0nx-=OV6vN2pG_3qA&T`[Kw^8yWbdN}xS9D[:05/?+8<)Uss4,qe?h4!0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
